.. |label| replace:: 1st Vision FormularErweiterung
.. |snippet| replace:: FvFormExtension
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.6.0
.. |maxVersion| replace:: 5.6.10
.. |Version| replace:: 1.0.0
.. |php| replace:: 7.4


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Im Shop wird es eine Infoseite geben mit einem Formular. Hier kann der Kunde das Formular ausfüllen und eine E-Mail geht an die hinterlegt E-Mail Adresse. Die E-Mail bekommt auch der Kunde der das Formular ausfüllt. Es wird auch eine upload-Funktion bereitgestellt, wo der Kunde eine Datei mit hochladen kann. Im Backend kann definiert werden welche Endungen die Dateien haben dürfen und wie groß Sie sein können. 


Backend
-------

.. image:: FvFormExtension1.png


:Erlaube Dateiendungen (Standard): Standard ist jpg;png;bmp;pdf weitere Formate können hinterlegt werden die als Upload im Formular erlaubt sind.
:Maximale Dateigröße (in MB): Standard ist 32, Sie können die Dateigröße hier Ändern die als Upload im Formular erlaubt ist


Formular
------

.. image:: FvFormExtension2.png

Hier kann ein neuer Typ "file" verwendet werden

.. image:: FvFormExtension3.png

:CC: Hier kann ein weitere Empfänger der Email hinterlegt werden (wenn Sie mehrere auf CC setzen wollen, dann mit Semikolon getrennt).
:Auch an eingetragene E-Mail-Adresse im Formular versenden: Der Ausfüller bekommt hier diese Email ebenso zugesendet.




